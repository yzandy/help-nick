/**
 * Created by jodychen on 11/22/14.
 */

// json object normally in different file, but for now, put in same file for faster i/o.
var projectObj = [
    {
        "Project": "Comixology - E-commerce Store",
        "Wins": ["User feedback was tremendous - within months of launching, hundreds of users sent in messages stating how much they enjoyed the new website", 
                "Within weeks of launching, more unique users visited the website then ever before",
                "Due to this, Comixology designated their webstore as their primary domain",
                "Revenue generated from new website doubled"],
        "Image": "comix/homepage9-400.png",
        "Problem": "The comic webstore user experience was very different from their mobile applications. Because the webstore was unwieldy, too many users were going instead to the company's iphone app where 30% of the sale had to be given to Apple.  There wasn’t any information architecture or content strategy in place, making the site difficult to navigate for users.",
        "Solution": "I identified the need and spearheaded the effort to bring consistency to the customer experience of the webstore, mobile apps, and windows apps.  This was a strategic need, because purchases made on the webstore were 30% more profitable for Comixology. We decided to encourage smartphone users up to the iphone 3g to use the mobile browser webstore instead of the iphone app. I then implemented a dual prong approach by creating a tablets/desktops website and a small form-factor mobile site, and leveraged the strengths of the desktop/tablet browser by increasing the content area and including related sections. I created a streamlined, well-delineated shopping cart flow. And I designed a content strategy that enabled discovery of related comics and optimized prime screen estate.  Now the webstore’s flow was tailored for the shopper instead of the retailer."
    },
    {
        "Project": "Marsh & McLennan - AutoID Web Application",
        "Wins": ["New web application is much more powerful and flexible, introducing features that the original application didn’t have",
                "Managers and legal can now easily audit and check for any costly mistakes",
                "Resources previously required for employee training were greatly reduced"],
        "Image": "marsh-auto/sitemap-400.png",
        "Problem": "Corporate car fleet insurance certificates were issued via an old terminal application that was inflexible. Employees spent significant amounts of time onboarding. The application didn’t have a clear workflow so employees made mistakes and needed to start over.  In addition, it lacked auditing tools, so managers and legal couldn’t quickly access any red flags.",
        "Solution": "I created the information architecture and interaction flow for a new intranet web application that is convenient for concurrent users located in multiple regions of the country. Users included managers, customer service representatives, and the legal department. The new user experience was intuitive for beginners and flexible for power users. Many regulations are involved with auto insurance, so a clear navigation structure was essential to prevent costly mistakes. I also designed auditing tools for managers and the legal department.  Wireframes and prototypes iteratively delivered, ensuring that the experience and features met user needs."
        },
    {
        "Project": "Marsh & McLennan - Intranet Portal",
        "Wins": ["New intranet contains much more content, which is more findable and searchable",
                "Employees can quickly find and assist each other",
                "Web applications now have a home and can easily be added to the intranet"],
        "Image": "marsh-intranet/global-home-desktop-400.png",
        "Problem": "The very basic intranet portal, which showed mostly HR information, didn’t account for the many localities and business units of the corporation. Colleagues in business units and localities were effectively siloed since there wasn’t a centralized place they could turn to for knowledge bases, intranet web apps, and networking.",
        "Solution": "For this project as well, I was responsible for the information architecture, interaction flow, and usability testing.  The new intranet arose from a multi-national effort, spanning teams from Singapore, Australia, the UK, and India. U.S. business stakeholders included multiple business units with various functions and team members included multiple project managers, business analysts, and developers. The new corporate intranet was to include vast repositories of knowledge articles and intranet web apps, so I enhanced findability and searchability. I also collaborated with a content strategist to create the new site’s taxonomy.  Now colleagues around the world can see their intranet as their computers start up. It has become much easier for colleagues to find and help each other."
    },
    {
        "Project": "Standard & Poors - Intranet Application Access",
        "Wins": [],
        "Image": "sp-opdm/Page_1-400.png",
        "Problem": "The web application had been designed much like a pre-existing paper form that employees used to make one-off requests for intranet application access and permissions. A lack of clear workflow meant that users had to guess which fields to click. The application didn’t convey which permissions a user currently had and what requests were in progress, creating many duplicates. The development and operations teams were spending too much time and energy on customer complaints as a result.",
        "Solution": "I created a structured workflow that guided users, preventing mistakes and delays.  With clear workflows for different types of users, each one can easily check their level of access and can quickly see a summary of changes before investing significant time filling out forms. Users can quickly differentiate what they intend to do: request, update, or revoke. The new user experience easily accommodates multiple applications and fine tuning of permissions for each request. The timeframe for this project was limited, so I also created a road-map of enhancements for the near and long term."
    },
    {
        "Project": "Chan Meditation Center - NGO Multimedia Site",
        "Wins": ["Contributors can now easily create, maintain, and edit content. The site can now organically grow from multiple contributors.",
                "By coding and employing SEO best practices, the site became second search result after ‘meditation queens’ and fourth after ‘meditation new york’. Before, it was ranked 8 or 9 in the search results.",
                "New website serves community better by having their rich multimedia assets online and thereby attract more interest. People who can’t attend the events live also have a chance to look at the video/audio"],
        "Image": "cmc/photo-album-400.png",
        "Problem": "This was a static HTML site with a 90s look and feel that could only be updated via hand coding. Navigation and styles were inconsistent since pages were maintained without a backend. They had significant amounts of multimedia assets such as video, audio, photographs, and pdf ebooks, yet lacked a content strategy when bringing them online. The site failed to convey the multitude of activities and events they were holding for the community even though their audience was numerous.",
        "Solution": "I guided the implementation of a cross-browser-compatible website with JQuery, sIFR fonts, and WordPress backend.  Since this was a small NGO, I used whiteboards and meetings to guide the graphics designer as to user experience principles and interaction best practices.  I created the site’s information architecture and a production quality prototype.  The developer was able to bind the new front end to his back end with minimal effort. This organization can now easily update their blogs, audio, video, and pdfs using the  WordPress CMS."
    },
    {
        "Project": "CompuDance - Responsive B2B Web Application",
        "Wins": ["Clients can maintain their studio website both on desktop and on the go mobile. The user experience is the similar, so ramp up time is greatly reduced.",
            "By creating a responsive site, all code is unified. Developers can create and add features faster instead of having to propagate changes across different platforms."],
        "Image": "compu-d/compu-ipad-landscape-400.png",
        "Problem": "This company provided  turn-key websites for martial arts studios, yoga studios, and dance studios.  Their users needed a consistent user experience across multiple devices in order to update their websites quickly and easily.",
        "Solution": "By focusing first on the tablet experience, I was able to create consistent, intuitive interactions and navigation for smartphone, tablet, and desktop screens. I greatly simplified smartphone navigation and content: we were targeting the iphone 4s. Using Bootstrap’s flexible css framework, I created a flexible prototype that works for tablets in portrait and landscape mode, mobiles in landscape, and desktop Firefox, Safari, Chrome, and IE with resolutions from 800 px across to 1900 px."
    }
];

var detailObjs = [ 
    {
        "Dir": "comix",
        "Images": ["cart-flow-500", "mobile-combo-500", "pagination-500", "comix-ad-hoc2-500", "comix-detail-screen6-500", "comix-home-500", "comix-series-detail-500", "detail-screen6-500" ]
    },
    {
        "Dir": "marsh-auto",
        "Images": ["home-results-500", "manage-contacts-500", "vin-edit-500", "vin-history-500", "wizard-step1-500", "wizard-step2-500" ]
    },
    {
        "Dir": "marsh-intranet",
        "Images": ["archive-desktop-550", "archive-tablet-550", "article-desktop-550", "article-tablet-550", "global-home-tablet-550"]
    },
    {
        "Dir": "sp-opdm",
        "Images": ["page-2-550", "Page_3-550", "process-diagram-500" ]
    },
    {
        "Dir": "cmc",
        "Images": ["audio-550", "directions-550", "eight-fold-moves-550", "event-blog-550", "literature-550", "magazine-550", "meditation-howto-550", "weekly-activities-550" ]
    },
    {
        "Dir": "compu-d",
        "Images": ["compu-ipad-landscape-500", "compu-ipad-portrait-500", "compu-iphone-portrait-500", "iphone-register-wireframe-500", "registration-form-500" ]
    }
    ];

var portfolioApp = angular.module('portfolioApp',  ['ngRoute', 'ui.bootstrap']);

portfolioApp
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'html/landing-ang.html',
                controller: 'projectListCtrl'
            })
            .when('/detail/:projId', {
                templateUrl: 'html/detail-ang.html',
                controller: 'projectListCtrl'
            })

            .when('/landing', {
                templateUrl: 'html/landing-ang.html',
                controller: 'projectListCtrl'
            })
            .when('/about', {
                templateUrl: 'html/about.html',
                controller: 'projectListCtrl'
            })
            .when('/thankyou', {
                templateUrl: 'html/thankyou.html',
                controller: 'projectListCtrl'
            })
            .when('/test', {
               templateUrl: 'html/testing-ground.html',
                controller: 'projectListCtrl'
             })
            .otherwise({
                redirectTo: '/'
            });
        // eventually, we could consider using $locationProvider to get rid of the hashbag for more seo compatibility.
        // $locationProvider.html5Mode(true);
        })
    .controller('navCtrl', function ($scope, $log, $location) {
        $log.log('within nav function');
        $scope.isCollapsed = true;
        $scope.menuClass = function(viewLocation) {
            if (viewLocation === $location.path() ) { 
                return "active";
            }
            else if (viewLocation === '/' && '/landing' === $location.path()) {
                return "active";
            }
            else if (viewLocation === '/' && '/detail/' === $location.path().substring(0, 8) ) {
                return "active";
            }
            else if (viewLocation === '/about' && '/thankyou' === $location.path().substring()) {
                return "active";
            }
            else {
                //$log.log ('location.path = ' + $location.path() + '  viewLocation = ' + viewLocation);
                return ""; 
            }
        };
    })
    .controller('projectListCtrl', function ($scope, $routeParams) {
/*      if we are retrieving $scope.projects via ajax
        $http.get('js/projects.json').success (function(data){
            $scope.projects = data;
        });
*/
        $scope.projects = projectObj;
        $scope.params = $routeParams;
    })
    .controller('CarouselCtrl', function ($scope, $routeParams) {
        $scope.myInterval = 5000;
        $scope.params = $routeParams;
        $scope.images = detailObjs[$scope.params.projId];
        var slides = $scope.slides = [];  
        $scope.addSlide = function(i) {
            slides.push({
                image: 'img/' + $scope.images.Dir + '/' + $scope.images.Images[i] + '.png'
                // image: 'http://placekitten.com/' + newWidth + '/300',
                // image: 'img/open-source/' +
                // ['cabin', 'cake', 'circus', 'game'][slides.length % 4] + '.png',
                // text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
                // ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
            });
        };
        for (var i=0; i<$scope.images.Images.length; i++) {
            $scope.addSlide(i);
        };
});
